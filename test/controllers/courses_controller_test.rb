require 'test_helper'

class CoursesControllerTest < ActionController::TestCase
  test "should get developer" do
    get :developer
    assert_response :success
  end

  test "should get doctor" do
    get :doctor
    assert_response :success
  end

  test "should get researcher" do
    get :researcher
    assert_response :success
  end

end
