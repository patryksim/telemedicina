class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :speciality
      t.text :address
      t.string :status

      t.timestamps null: false
    end
  end
end
