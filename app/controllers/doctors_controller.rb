class DoctorsController < ApplicationController
before_action :authenticate_user!

  def index
    @doctors = Doctor.all
  end

  def new
  end

  def edit
  end

  def show
    @doctor = Doctor.find(params[:id])
  end

  def create
    @doctor = Doctor.new(doctor_params)
    @doctor.save
    redirect_to @doctor
  end

  private
  def doctor_params
    params.require(:doctor).permit(:name, :email, :phone, :speciality, :address)
  end

end
