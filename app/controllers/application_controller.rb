class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception


  def after_sign_in_path_for(resource)
    logger.debug "EN EL AFTER DEL SIGN IN"
    if session[:bc_wait_action]
      create_get_events_path params: session[:bc_params]
    else
      root_path
    end
  end

  def after_sign_up_path_for(resource)
    logger.debug "EN EL AFTER DEL SIGN IN"
    if session[:bc_wait_action]
      create_get_events_path params: session[:bc_params]
    else
      root_path
    end
  end
end
